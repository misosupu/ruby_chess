describe Queen do
  describe '#new' do
    it 'creates a new Queen object' do
      instance = Queen.new(6, 6, :black)
      expect(instance).to be_an_instance_of Queen
      expect(instance).to respond_to(:position)
      expect(instance).to respond_to(:colour)
      expect(instance).to respond_to(:movement_directions)
      expect(instance.colour).to eq :black
    end
  end

  describe '#get_moves' do
    let(:board) do
      Board.new (
        [
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, Queen.new(1, 1, :white), nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [Pawn.new(3, 0, :white), nil, nil, nil, nil, nil, nil, nil],
          [Queen.new(4, 0, :white), nil, nil, nil, nil, nil, Pawn.new(4, 6, :black), nil],
          [Pawn.new(5, 0, :white), nil, nil, Bishop.new(5, 3, :white), nil, nil, nil, Queen.new(5, 7, :black)],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil]
        ])
    end

    it 'returns correct move coordinates when no obstacles are present' do
      figure = board[1, 1]
      result = [[0, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1], [1, 0],
                [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7], [0, 0], [2, 2],
                [3, 3], [4, 4], [5, 5], [6, 6], [7, 7], [2, 0], [0, 2]]
      expect(figure).to be_instance_of Queen
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns correct move coordinates when obstacles are present' do
      figure = board[5, 7]
      result = [[4, 7], [3, 7], [2, 7], [1, 7], [0, 7], [6, 7],
                [7, 7], [5, 6], [5, 5], [5, 4], [5, 3], [6, 6], [7, 5]]
      expect(figure).to be_instance_of Queen
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns no available moves when cornered' do
      figure = board[4, 0]
      result = [[4, 1], [4, 2], [4, 3], [4, 4], [4, 5], [4, 6],
                [5, 1], [6, 2], [7, 3], [3, 1], [2, 2], [1, 3], [0, 4]]
      expect(figure).to be_instance_of Queen
      expect(figure.get_moves(board)).to eq result
    end
  end
end