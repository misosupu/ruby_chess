require_relative 'spec_helper'

describe Knight do
  describe '#new' do
    it 'creates a new knight object' do
      instance = Knight.new(0, 0, :white)
      expect(instance).to be_an_instance_of Knight
      expect(instance).to respond_to(:position)
      expect(instance).to respond_to(:colour)
      expect(instance.colour).to eq :white
    end
  end

  describe '#get_moves' do
    let(:board) do
      Board.new (
      [
          [nil, nil, nil, Pawn.new(0, 4, :white), nil, nil, nil, nil],
          [nil, Knight.new(1, 1, :black), nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, Pawn.new(2, 4, :black), nil, nil, nil, nil],
          [Bishop.new(4, 0, :black), nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, Bishop.new(4, 4, :white), nil, nil, nil],
          [nil, nil, nil, Knight.new(5, 3, :black), nil, nil, Pawn.new(5, 6, :white), nil],
          [nil, nil, nil, nil, nil, Pawn.new(6, 5, :white), nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, Knight.new(7, 7, :white)]
      ])
    end
    it 'returns all possible moves when no collisions are present' do
      figure = board[5, 3]
      result = [[3, 2], [3, 4], [7, 4], [7, 2], [6, 1], [6, 5], [4, 5], [4, 1]]
      expect(figure).to be_instance_of Knight
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns all possible moves when collisions are present' do
      figure = board[1, 1]
      expect(figure).to be_instance_of Knight
      expect(figure.get_moves(board)).to eq [[3, 2], [0, 3]]
    end

    it 'returns no possible moves when cornered by allies' do
      figure = board[7, 7]
      expect(figure).to be_instance_of Knight
      expect(figure.get_moves(board)).to eq []
    end
  end
end
