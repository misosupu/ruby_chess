require 'simplecov'
SimpleCov.start

require_relative '../lib/pieces/piece'
require_relative '../lib/pieces/bishop'
require_relative '../lib/pieces/knight'
require_relative '../lib/pieces/rook'
require_relative '../lib/pieces/king'
require_relative '../lib/pieces/queen'
require_relative '../lib/pieces/pawn'
require_relative '../lib/pieces/board'
require_relative '../lib/utilities/board_inspector'
require_relative '../lib/game_engine'
require_relative '../lib/utilities/notation_parser'
require_relative '../lib/console/input_parser'
require_relative '../lib/utilities/fen_parser'
require_relative '../lib/utilities/lan_parser'

# require 'factory_girl'

RSpec.configure do |config|
  # Use color in STDOUT
  config.color = true

  # Use color not only in STDOUT but also in pagers and files
  config.tty = true

  # Use the specified formatter
  config.formatter = :documentation # :progress, :html, :textmate
end
