describe Board do
  before(:each) do
    @board = Board.new (
      [
        [nil, nil, nil, Pawn.new(0, 3, :white), nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, King.new(2, 6, :black), nil],
        [nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, King.new(4, 1, :white), nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, Pawn.new(6, 1, :white), nil, nil, nil, nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil]
      ])
  end

    it 'retrieves board objects correctly' do
      figure = @board[0, 3]
      expect(figure).to be_instance_of Pawn
      expect(figure.position.row).to eq 0
      expect(figure.position.column).to eq 3
    end


    it 'is able to retrieve the white king' do
      expect(@board.locate_king(:white).position.row).to eq 4
      expect(@board.locate_king(:white).position.column).to eq 1
    end

    it 'is able to retrieve the black king' do
      expect(@board.locate_king(:black).position.row).to eq 2
      expect(@board.locate_king(:black).position.column).to eq 6
    end

    it 'retrieves correctly all the pieces of a certain colour' do
      expect(@board.pieces(:white).length).to eq 3
    end
end
