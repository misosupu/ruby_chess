require_relative 'spec_helper'

describe Bishop do
  describe '#new' do
    it 'creates a new bishop object' do
      instance = Bishop.new(3, 4, :white)
      expect(instance).to be_an_instance_of Bishop
      expect(instance).to respond_to(:position)
      expect(instance).to respond_to(:colour)
      expect(instance.colour).to eq :white
    end
  end

  describe '#get_moves' do
    let(:board) { Board.new(
      [
        [Bishop.new(0, 0, :black), nil, nil, nil, nil, nil, nil, Bishop.new(0, 7, :white)],
        [nil, Rook.new(1, 1, :black), nil, nil, Queen.new(1, 4, :black), nil, nil, nil],
        [nil, nil, nil, nil, Bishop.new(2, 4, :black), nil, nil, nil],
        [nil, nil, nil, nil, nil, nil, nil, nil],
        [nil, Pawn.new(4, 1, :white), nil, nil, Bishop.new(4, 4, :white), nil, nil, nil],
        [nil, nil, nil, Knight.new(5, 3, :black), nil, nil, nil, nil],
        [nil, nil, Pawn.new(6, 2, :white), nil, Pawn.new(6, 4, :white), nil, nil, nil],
        [nil, nil, nil, Bishop.new(7, 3, :black), nil, nil, nil, nil]
      ]) }
    it 'returns correct possible moves when other figures are present' do
      figure = board[4, 4]
      expect(figure).to be_instance_of Bishop
      result = [[1, 1], [1, 7], [2, 2], [2, 6], [3, 3],
                [3, 5], [5, 3], [5, 5], [6, 6], [7, 7]]
      expect(figure.get_moves(board).sort).to eq result
    end

    it 'returns correct possible moves if no other figures are present' do
      figure = board[2, 4]
      expect(figure).to be_instance_of Bishop
      result = [[1, 3], [0, 2], [3, 5], [4, 6], [5, 7],
                [3, 3], [4, 2], [5, 1], [6, 0], [1, 5], [0, 6]]
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns correct possible moves when at corner' do
      figure = board[0, 7]
      expect(figure).to be_instance_of Bishop
      result = [[1, 6], [2, 5], [3, 4], [4, 3], [5, 2], [6, 1], [7, 0]]
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns correct number of moves, when corned by opponents' do
      figure = board[7, 3]
      expect(figure).to be_instance_of Bishop
      expect(figure.get_moves(board)).to eq [[6, 2], [6, 4]]
    end

    it 'returns no moves when cornered by allies' do
      figure = board[0, 0]
      expect(figure).to be_instance_of Bishop
      expect(figure.get_moves(board)).to eq []
    end
  end
end