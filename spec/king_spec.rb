describe King do
  describe '#new' do
    it 'creates a new King object' do
      instance = King.new(3, 3, :black)
      expect(instance).to be_an_instance_of King
      expect(instance).to respond_to(:position)
      expect(instance).to respond_to(:colour)
      expect(instance.colour).to eq :black
    end
  end

  # describe '#to_s' do
  #   it 'prints out correct ASCII character' do
  #     black_king = King.new(0, 0, :black)
  #     white_king = King.new(7, 7, :white)
  #     expect(black_king.to_s).to eql '♚'
  #     expect(white_king.to_s).to eql '♔'
  #   end
  # end

  describe '#get_moves' do
    let(:board) do
      Board.new (
        [
          [nil, nil, nil, Pawn.new(0, 3, :white), nil, nil, nil, nil],
          [nil, Knight.new(1, 1, :black), nil, nil, nil, nil, King.new(1, 6, :white), nil],
          [nil, nil, nil, Pawn.new(2, 4, :black), nil, nil, nil, nil],
          [Bishop.new(4, 0, :black), nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, Bishop.new(4, 4, :white), nil, nil, nil],
          [nil, nil, nil, Knight.new(5, 3, :black), nil, nil, Pawn.new(5, 6, :white), nil],
          [Bishop.new(6, 0, :black), Queen.new(6, 1, :black), nil, nil, nil, Pawn.new(6, 5, :white), nil, nil],
          [King.new(7, 0, :black), Pawn.new(7, 1, :black), nil, nil, nil, King.new(7, 5, :white), Knight.new(7, 7, :white), nil]
        ])
    end
    it 'returns correct move coordinates when no obstacles are present' do
      figure = board[1, 6]
      result = [[2, 6], [0, 6], [1, 5], [1, 7], [0, 5], [2, 7], [2, 5], [0, 7]]
      expect(figure).to be_instance_of King
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns correct move coordinates when obstacles are present' do
      figure = board[7, 5]
      result = [[7, 4], [6, 4], [6, 6]]
      expect(figure).to be_instance_of King
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns no possible move coordinates when cornered' do
      figure = board[7, 0]
      expect(figure).to be_instance_of King
      expect(figure.get_moves(board)).to eq []
    end
  end
end