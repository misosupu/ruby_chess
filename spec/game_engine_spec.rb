describe GameEngine do
  describe '#new' do
    it 'initializes a chess game with white as first player' do
      instance = GameEngine.new
      expect(instance.current_player).to eq :white
    end
  end

  describe '#move_figure' do
    it 'successfully completes a coup' do
      board =
          [
            [nil, nil, nil, nil, Bishop.new(0, 4, :white), nil, nil, nil],
            [nil, nil, nil, Bishop.new(1, 3, :black), nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, King.new(3, 1, :black), nil, nil, nil, Queen.new(3, 5, :white), nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, nil, nil, nil],
            [nil, nil, nil, nil, nil, King.new(7, 5, :white), Knight.new(7, 7, :white), nil]
          ]
      engine = GameEngine.new(board)
      engine.current_player = :white
      conqueror = engine.board[0, 4]
      conquered = engine.board[1, 3]
      engine.move_figure([0, 4], [1, 3])
      expect(engine.last_taken).to eql conquered
      expect(engine.board[0, 4]).to eq nil
      expect(engine.board[1, 3]).to eq conqueror
    end
  end

  describe '#rollback' do
    it 'rolls back to the previous position after an invalid one' do
      board =
          [
              [nil, nil, nil, nil, Bishop.new(0, 4, :white), nil, nil, nil],
              [nil, nil, nil, Bishop.new(1, 3, :black), nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, King.new(3, 1, :black), nil, nil, nil, Queen.new(3, 5, :white), nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, King.new(7, 5, :white), Knight.new(7, 7, :white), nil]
          ]
      engine = GameEngine.new(board)
      engine.current_player = :black
      bishop = board[1, 3]
      queen = board[3, 5]
      # bishop takes the white Queen
      expect {
          engine.move_figure([1, 3], [3, 5])
        }.to raise_error  # bishop takes the white queen
      expect(board[1, 3]).to eq bishop
      expect(board[3, 5]).to eq queen
    end
  end

  describe '#move_figure' do
    it 'raises exception when an invalid move is made' do
      board =
          [
              [Bishop.new(0, 0, :white), nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, King.new(3, 1, :black), nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, nil, nil, nil],
              [nil, nil, nil, nil, nil, King.new(7, 5, :white), nil, nil]
          ]
      engine = GameEngine.new(board)
      engine.current_player = :white
      bishop = board[0, 0]
      expect {
        engine.move_figure([0, 0], [0, 1])
      }.to raise_error('Invalid move')
    end
  end
end
