describe InputParser do
  describe '#validate_input' do

    # positive test
    it 'validates correct coordinates' do
      inputs = %w(13 27 75 81 11)
      puts inputs
      # expect(inputs.all?(&:InputParser.validate_input)).to eq true
      expect(inputs.all? { |input| InputParser.validate_input(input) }).to eq true
    end

    # negative test
    it 'rejects incorrect coordinates' do
      inputs = %w(14 91 05 22 49)
      expect(inputs.all? { |input| InputParser.validate_input(input) }).to eq false
    end
  end

  describe '#decision_valid?' do

    # positive test
    it 'validates correct decision input' do
      inputs = [['3', :turn], ['1', :newgame],
                ['1', :turn], ['2', :newgame]]
      expect(inputs.all? { |input, type| InputParser.decision_valid?(input, type) } ).to eq true
    end

    # negative test
    it 'rejects incorrect decision input' do
      inputs = [['3', :newgame], ['1', :turn],
                ['0', :newgame], ['7', :turn]]
      expect(inputs.any? { |input, type| InputParser.decision_valid?(input, type) } ).to eq true
    end
  end

  describe '#parse' do

    # positive test
    it 'parses correct number coordinates' do
      inputs = %w(12 32 99 62 70 01)
      expect(inputs.all? { |input| InputParser.parse(input) }).to eq true
    end

    # positive test
    it 'returns a two-element array' do
      inputs = %w(12 32 99 62 70 01)
      expect(inputs.all? { |input| InputParser.parse(input).length == 2 }).to eq true
    end
  end
end