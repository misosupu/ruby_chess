describe NotationParser::FenParser do
  describe '#fen_to_board' do
    it 'correctly parses fen string' do
      fen_string = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR'
      converted_board = NotationParser::FenParser.fen_to_board(fen_string)
      expect(converted_board).to eq Board.new.board
    end
  end

  describe '#board_to_fen' do
    it 'converts board to fen string correctly' do
      board = Board.new(
        [
         [nil, nil, nil, Pawn.new(0, 3, :white), nil, nil, nil, nil],
         [nil, nil, nil, nil, nil, nil, nil, nil],
         [nil, nil, nil, nil, nil, nil, King.new(2, 6, :black), nil],
         [nil, nil, nil, nil, nil, nil, nil, nil],
         [nil, King.new(4, 1, :white), nil, nil, nil, nil, nil, nil],
         [nil, nil, nil, nil, nil, nil, nil, nil],
         [nil, Pawn.new(6, 1, :white), nil, nil, nil, nil, nil, nil],
         [nil, nil, nil, nil, nil, nil, nil, nil]
        ])
      fen = '3P4/8/6k1/8/1K6/8/1P6/8'
      converted_string = NotationParser::FenParser.board_to_fen(board.board)
      expect(converted_string).to eq fen
    end
  end
end