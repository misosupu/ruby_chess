require_relative 'spec_helper'

describe Piece do
  describe '#new' do
    it 'should create piece object' do
      instance = Piece.new(1, 2, :white)
      expect(instance).to respond_to(:position, :colour)
      expect(instance.colour).to eq :white
      expect(instance.position.row).to eq 1
      expect(instance.position.column).to eq 2
    end
  end
end