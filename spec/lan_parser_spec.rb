describe NotationParser::LanParser do
  describe '#parse_move' do
    it 'converts standard coordinates into LAN notation' do
      piece = Bishop.new(3, 3, :black)
      source = [3, 3]
      target = [6, 6]
      expect(NotationParser::LanParser.parse_move(piece, source, target)).to eq 'b33-66'
    end
  end
end