require_relative 'spec_helper'

describe Rook do
  describe '#new' do
    it 'creates a new Rook object' do
      instance = Rook.new(0, 0, :white)
      expect(instance).to be_an_instance_of Rook
      expect(instance).to respond_to(:position)
      expect(instance).to respond_to(:colour)
      expect(instance).to respond_to(:movement_directions)
      expect(instance).to respond_to(:movement_type)
      expect(instance.colour).to eq :white
    end
  end

  describe '#get_moves' do
    let(:board) do
      Board.new(
        [
          [nil, nil, nil, Pawn.new(0, 4, :white), nil, nil, nil, nil],
          [nil, Knight.new(1, 1, :black), nil, nil, nil, nil, nil, nil],
          [nil, nil, Pawn.new(2, 3, :black), nil, nil, nil, nil, nil],
          [Bishop.new(4, 0, :black), nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, Rook.new(4, 2, :black), nil, Bishop.new(4, 4, :white), nil, nil, nil],
          [nil, nil, nil, Knight.new(5, 3, :white), nil, nil, nil, nil],
          [nil, nil, Queen.new(6, 2, :white), Rook.new(6, 3, :white), King.new(6, 4, :white), Pawn.new(6, 5, :white), nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, Rook.new(7, 7, :white)]
        ])
    end

    it 'returns correct moves when no obstacles are present' do
      figure = board[7, 7]
      result = [[6, 7], [5, 7], [4, 7], [3, 7], [2, 7], [1, 7], [0, 7],
                [7, 6], [7, 5], [7, 4], [7, 3], [7, 2], [7, 1], [7, 0]]
      expect(figure).to be_instance_of Rook
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns correct moves when obstacles are present' do
      figure = board[4, 2]
      result = [[3, 2], [5, 2], [6, 2], [4, 1], [4, 0], [4, 3], [4, 4]]
      expect(figure).to be_instance_of Rook
      expect(figure.get_moves(board)).to eq result
    end

    it 'returns no moves when cornered by 3 sides' do
      figure = board[6, 3]
      expect(figure).to be_instance_of Rook
      expect(figure.get_moves(board)).to eq [[7, 3]]
    end
  end
end