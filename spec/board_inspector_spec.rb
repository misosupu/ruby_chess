describe BoardInspector do
  describe '#check?' do

    # positive test
    it 'identifies a check situation' do
      board = Board.new([
          [nil, nil, King.new(0, 2, :black), nil, Bishop.new(0, 4, :black), nil, nil, nil],
          [Pawn.new(1, 0, :white), Rook.new(1, 1, :white), Rook.new(1, 2, :white), nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, King.new(3, 5, :white), nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil]
      ])
      king = board[0, 2]
      expect(BoardInspector.check?(board, king)).to eq true
    end

    # negative test
    it 'does not report check when king is not endangered' do
      board = Board.new([
          [nil, nil, King.new(0, 2, :black), nil, Bishop.new(0, 4, :black), nil, nil, nil],
          [Pawn.new(1, 0, :white), Rook.new(1, 1, :white), nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, Rook.new(3, 5, :white), nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, King.new(7, 3, :white), nil, nil, nil, nil]
      ])
      king = board[0, 2]
      expect(BoardInspector.check?(board, king)).to eq false
    end
  end

  describe '#check_mate?' do

    # positive test
    it 'identifies a check-mate situation' do
      board = Board.new([
          [nil, nil, nil, Rook.new(0, 3, :white), nil, nil, King.new(0, 6, :black), nil],
          [nil, nil, nil, nil, nil, Pawn.new(1, 5, :black), Pawn.new(1, 6, :black), Pawn.new(1, 7, :black)],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, King.new(7, 7, :white)]
      ])
      king = board[0, 6]
      king_threats = BoardInspector.threats(board, king)
      # puts "Threats: #{king_threats} "
      expect(BoardInspector.check_mate?(board, king, king_threats)).to eq true
    end

    # negative test
    it 'ignores situations when there is no check-mate' do
      board = Board.new([
          [King.new(0, 0, :black), nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, Queen.new(5, 2, :white), nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [Rook.new(7, 0, :black), nil, nil, nil, King.new(7, 4, :white), nil, nil, nil]
      ])
      king = board[7, 4]
      king_threats = BoardInspector.threats(board, king)
      expect(BoardInspector.check_mate?(board, king, king_threats)).to eq false
    end
  end

  describe '#game_over?' do

    # positive test
    it 'identifies when the game is over' do
      board = Board.new([
          [nil, nil, nil, Rook.new(0, 3, :white), nil, nil, King.new(0, 6, :black), nil],
          [nil, nil, nil, nil, nil, Pawn.new(1, 5, :black), Pawn.new(1, 6, :black), Pawn.new(1, 7, :black)],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, King.new(7, 7, :white)]
      ])
      player = :black
      expect(BoardInspector.game_over?(board, player)).to eq true
    end
  end

  describe '#move_valid?' do

    before(:each) do
      @board = Board.new([
          [nil, nil, nil, King.new(0, 3, :black), nil, nil, nil, nil],
          [nil, nil, Rook.new(1, 3, :white), nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, Queen.new(6, 7, :white)],
          [nil, nil, nil, nil, nil, nil, nil, King.new(7, 7, :white)]
      ])
    end

    it 'allows movement of players figures' do
      expect(BoardInspector.move_valid?(@board, [0, 3], :black, :from)).to eq true
    end

    it 'rejects movement of enemy figures' do
      expect(BoardInspector.move_valid?(@board, [7, 7], :black, :from)).to eq false
    end

    it 'allows overtaking enemy units (coup)' do
      expect(BoardInspector.move_valid?(@board, [1, 3], :black, :to)).to eq true
    end

    it 'rejects overtaking own units' do
      expect(BoardInspector.move_valid?(@board, [6, 7], :black, :to)).to eq true
    end
  end
end
