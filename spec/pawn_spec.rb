describe Pawn do
  describe '#new' do
    it 'creates a new Pawn object' do
      instance = Pawn.new(4, 7, :white)
      expect(instance).to be_an_instance_of Pawn
      expect(instance).to respond_to(:position)
      expect(instance).to respond_to(:colour)
      expect(instance.colour).to eq :white
    end
  end

  describe '#get_moves' do
    let(:board) do
      Board.new (
        [
          [nil, nil, nil, Pawn.new(0, 3, :white), nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, Pawn.new(2, 4, :black), nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, Pawn.new(4, 3, :white), nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil],
          [nil, Pawn.new(6, 1, :white), nil, nil, nil, nil, nil, nil],
          [nil, nil, nil, nil, nil, nil, nil, nil]
        ])
    end

    it 'returns all available moves when no opponent is intersecting' do
      figure = board[6, 1]
      puts "Used two square advance? #{figure.used_two_square_advance?}"
      expect(figure).to be_instance_of Pawn
      expect(figure.get_moves(board)).to eq [[5, 1], [4, 1]]
    end

    it 'fails to move when reached the end of the board' do
      figure = board[0, 3]
      expect(figure).to be_instance_of Pawn
      expect(figure.get_moves(board)).to eq []
    end

    it 'uses the special double-square advance rule correctly' do
      figure = board[6, 1]
      figure.move(5, 1)
      expect(figure.get_moves(board)).to eq [[4, 1]]
    end

    it 'can spot opponents which it can overtake' do
      figure = board[2, 4]
      figure.move(3, 4)
      expect(figure).to be_instance_of Pawn
      expect(figure.get_moves(board)).to eq [[4, 4], [4, 3]]
    end
  end
end