# Chess

> Chess is a two-player board game played on a chessboard, a checkered gameboard with 64 squares arranged in an eight-by-eight grid. Chess is played by millions of people worldwide, both amateurs and professionals.The objective is to 'checkmate' the opponent's king by placing it under an inescapable threat of capture.

Courtesy to [Wikipedia][df1]

This is an attempt at a basic chess game, `still in development`.

### Installation

To install the game and the proper gems, you must do the following:

```sh
$ git clone [repo-url] chess
$ cd chess
$ bundle install
```

### How to play
The game has two interfaces - console and web. To start either one of them, `cd` into the `bin` directory and run, respectively:

```
ruby console_play.rb
```
or

```
ruby web_play.rb
```

### How to run tests
Simply type
```sh
$ rake spec
```
in you command line.

### Documentation
Albeit poorly written and scarse to say the least, the documentation is still accessible in the `docs` folder. 

### Coverage
The coverage report can be found in the, appropriately named, `coverage` folder.

### Planned features

The planned features include
* actual frontend in the web view
* game state logging with sqlite3
* rewind functionality for logged games

### Version
0.0.9

   [df1]: <https://en.wikipedia.org/wiki/Chess>
   [marked]: <https://github.com/chjj/marked>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [keymaster.js]: <https://github.com/madrobby/keymaster>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]:  <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
