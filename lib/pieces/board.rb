require_relative '../../lib/pieces/pawn'
require_relative '../../lib/pieces/bishop'
require_relative '../../lib/pieces/rook'
require_relative '../../lib/pieces/knight'
require_relative '../../lib/pieces/king'
require_relative '../../lib/pieces/queen'


# require_relative '../../views/console'

# Keeps the current state of the game.
class Board

  attr_reader :board

  def initialize(board_given = nil)
    if !board_given.nil?
      @board = board_given
    else
      @board = []
      8.times { @board << Array.new(8, nil) }
      set
    end
  end

  def [](row, column)
    begin
      @board[row][column]
    rescue IndexError
      raise ChessError.new('Invalid board position')
    end
  end

  def []=(row, column, value)
    begin
      @board[row][column] = value
    rescue IndexError
      raise ChessError.new('Invalid board position')
    end
  end

  # Return reference to king object from said color
  def locate_king(colour)
    self.pieces(colour).find { |figure| figure.is_a?(King) }
  end

  # Return all the active pieces of said color
  def pieces(colour)
    @board.flatten.select { |figure| !figure.nil? && figure.colour == colour }
  end

  # Checks if the tile on row 'row' and column 'column' is taken
  def taken?(row, column)
    !@board[row][column].nil?
  end

  def set
    place_pawns
    place_royalties
  end

  # populates the board with pawn positions (whites row = 2, blacks row is 7)
  def place_pawns
    [[6, :white], [1, :black]].each do |row, colour|
      (0..7).each { |column| @board[row][column] = Pawn.new(row, column, colour) }
    end
  end

  # populates the board with royalty figures (whites row = 1, blacks row = 8)
  def place_royalties
    %w(Rook Knight Bishop Queen King Bishop Knight Rook).each_with_index do |figure, column|
      @board[7][column] = Kernel.const_get(figure).new(7, column, :white)
      @board[0][column] = Kernel.const_get(figure).new(0, column, :black)
    end
  end
end

# board = Board.new
# board.set
# # p board
# #
# ConsoleView.render(board.board)
