require_relative '../../lib/pieces/piece'

# Represents a bishop figure in standard chess terms.
class Bishop < Piece
  def initialize(row, column, colour)
    super(row, column, colour)
    @movement_directions = [[-1, -1], [1, 1], [1, -1], [-1, 1]]
    @movement_type = :continuous
  end
end