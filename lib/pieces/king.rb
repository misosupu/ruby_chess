require_relative 'piece'

# Represents a King figure in standard chess terms.
class King < Piece
  def initialize(row, column, colour)
    super(row, column, colour)
    @movement_directions = [[1, 0], [-1, 0], [0, -1], [0, 1],
                            [-1, -1], [1, 1], [1, -1], [-1, 1]]
    @movement_type = :discrete
  end
end