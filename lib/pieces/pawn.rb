require_relative '../../lib/pieces/piece'

# Represents a pawn figure.
class Pawn < Piece
  attr_reader :orientation

  def initialize(row, column, colour)
    super(row, column, colour)
    @orientation = determine_orientation
    @movement_directions = [[1 * @orientation, 0]]
    @moves = 0
  end

  def get_moves(board)
    moves = MovesGenerator::DiscreteMovesGenerator.moves(board, self)
    unless used_two_square_advance?
      goal = Coordinate.new(@position.row + 2 * @orientation, @position.column)
      unless MovesGenerator.cell_inaccessible?(board, self, goal, @colour)
      # apply special movement rule - move two squares from the initial position
        moves << [goal.row, goal.column]
      end
    end
    moves
  end

  def move(row, column)
    super(row, column)
    @moves += 1
  end

  def used_two_square_advance?
    @moves != 0
  end

  private

  def determine_orientation
    if colour == :white
      -1 # go 'up'
    else
      1 # go 'down'
    end
  end
end