require_relative '../pieces/piece'

# Represents a knight figure in standard chess terms.
class Knight < Piece
  def initialize(row, column, colour)
    super(row, column, colour)
    @movement_directions = [[-2, -1], [-2, 1], [2, 1], [2, -1],
                            [1, -2], [1, 2], [-1, 2], [-1, -2]]
    @movement_type = :discrete
  end
end