require_relative '../../lib/pieces/piece'

# Represents a queen figure in standard chess terms.
class Queen < Piece
  def initialize(row, column, colour)
    super(row, column, colour)
    @movement_directions = [[-1, 0], [1, 0], [0, -1], [0, 1],
                            [-1, -1], [1, 1], [1, -1], [-1, 1]]
    @movement_type = :continuous
  end
end