require_relative '../../lib/pieces/piece'

# Represents a rook figure in standard chess terms.
class Rook < Piece
  def initialize(row, column, colour)
    super(row, column, colour)
    @movement_directions = [[-1, 0], [1, 0], [0, -1], [0, 1]]
    @movement_type = :continuous
  end
end