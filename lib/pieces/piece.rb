require_relative '../../lib/utilities/coordinate'
require_relative '../../lib/utilities/moves_generator'

# Base class for all chess pieces
class Piece

  attr_reader :colour, :movement_directions, :movement_type
  attr_accessor :position

  def initialize(row, column, colour)
    @position = Coordinate.new(row, column)
    @colour = colour
    @movement_directions = nil
    @movement_type = nil
  end

  def ==(other)
    colour == other.colour && position == other.position
  end

  def move(row, column)
    @position.row = row
    @position.column = column
  end

  # Calculates the position rank from the point of view of piece color
  def rank
    if @colour == :white
      8 - @position.row - 1
    else
      @position.row + 1 # FIXME - magic number is magic
    end
  end

  def get_moves(board)
    if @movement_type == :continuous
      MovesGenerator::ContinuousMovesGenerator.moves(board, self)
    elsif @movement_type == :discrete
      MovesGenerator::DiscreteMovesGenerator.moves(board, self)
    end
  end
end