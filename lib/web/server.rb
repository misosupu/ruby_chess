require 'sinatra'

require_relative '../game_engine'
require_relative '../utilities/fen_parser'

class ApplicationServer < Sinatra::Application
  configure do
    @@engine = GameEngine.new
    @@black_score = 0
    @@white_score = 0
  end

  get '/' do
    fen_string = NotationParser::FenParser.board_to_fen(@@engine.board.board)
    erb :index, :locals => {:grid => fen_string, :current_player => @@engine.current_player,
                            :white_score => @@white_score, :black_score => @@black_score}
  end

  post '/sendmove' do
    current = NotationParser::FenParser.standard_to_coords(@params[:source])
    goal = NotationParser::FenParser.standard_to_coords(@params[:target])
    begin
      @@engine.move_figure(current, goal)
    rescue ChessError
      return 'snapback'
    end
    if @@engine.game_over?
      if @@engine.current_player == :white
        @@white_score += 1
      else
        @@black_score += 1
      end
      @@engine = GameEngine.new
      'gameover'
    end
  end
  run!
end
