require 'yaml'

require_relative 'pieces/board'
require_relative 'utilities/board_inspector'
require_relative 'utilities/chess_exceptions'

# Engine class for the chess game.
class GameEngine
  attr_reader :board, :last_taken, :players
  attr_accessor :current_player

  def initialize(initial_state = nil)
    @board = initial_state.nil? ? Board.new : Board.new(initial_state)
    @players = [:white, :black].cycle # white is always first
    # @current_player = @players.next
    @current_player = @current_player == :white ? :black : :white
    @last_taken = nil # keeps track of last taken piece to facilitate rollback
  end

  def game_over?
    BoardInspector.game_over?(@board, @current_player)
  end

  def save_game
    filename = File.expand_path('../../', __FILE__) +
              "/saves/game_#{Time.now.strftime('%d%m%Y_%H%M%S')}"
    data = { board: @board, player_turn: @current_player }
    File.open("#{filename}.yml", 'w') do
      |file| file.write(data.to_yaml) # TODO: catch exceptions
    end
    filename
  end

  def load_game(game_name)
    filename = File.expand_path('../../', __FILE__) + "/saves/#{game_name}.yml"
    parsed = begin
      YAML.load(File.open(filename))
    rescue ArgumentError => exception
      puts "Could not parse YAML: #{exception.message}"
    end
    @board = parsed[:board]
    @current_player = parsed[:player_turn]
  end

  # rewind the last move, return the piece to its previous position
  # if the last move was a coup - return the removed piece to the board
  def rollback(current, previous, move = :regular)
    @board[*current].move(*previous)
    @board[*previous] = @board[*current]
    @board[*current] = move == :coup ? @last_taken : nil
  end

  # moves figure from current position to the goal position
  # the move is invalid if it places the player in check
  def move_figure(current, goal)
    unless coordinate_valid?(current, :from) &&
           coordinate_valid?(goal, :to) &&
           @board[*current].get_moves(@board).include?(goal)
      raise ChessError.new('Invalid move')
    end
    if @board.taken?(*goal)
      @last_taken = @board[*goal]
      result = :coup
    end
    @board[*current].move(*goal)
    @board[*goal] = @board[*current]
    @board[*current] = nil
    king = @board.locate_king(@current_player)
    if BoardInspector.check?(@board, king)
      rollback(goal, current, result)
      raise ChessError.new('Player is in check')
    else
      # @current_player = @players.next
      @current_player = @current_player == :white ? :black : :white
    end
  end

  def coordinate_valid?(coordinate, direction)
      BoardInspector.move_valid?(@board, coordinate, @current_player, direction)
  end
end
