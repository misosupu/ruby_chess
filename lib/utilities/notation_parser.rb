module NotationParser
  FEN_NOTATION = { 'P' => 'Pawn', 'N' => 'Knight', 'R' => 'Rook',
                   'Q' => 'Queen', 'K' => 'King', 'B' => 'Bishop' }
end