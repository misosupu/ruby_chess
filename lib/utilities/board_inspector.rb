module BoardInspector

  def self.check?(board, king)
    !threats(board, king).empty?
  end

  def self.game_over?(board, player)
    king = board.locate_king(player)
    king_threats = threats(board, king)
    check?(board, king) && check_mate?(board, king, king_threats)
  end

  # Moves the kings to all possible positions and checks if it is in check.
  # Must hide the current position of the king
  # to avoid missed locations with the pieces with continuous movement pattern.
  def self.check_mate?(board, king, threats)
    if threats.any? { |attacker| can_avoid_threat?(board, attacker, king) }
      return false
    end
    possible_positions = []
    king_positions = king.get_moves(board)
    king_positions.each do |row, column|
      mock = king.clone
      # hide the king to avoid missed positions:
      board[king.position.row, king.position.column] = nil
      mock.position = king.position.clone
      mock.position.change(row, column)
      possible_positions << mock
    end
    flag_checkmate = possible_positions.all? { |king| check?(board, king) }
    board[king.position.row, king.position.column] = king
    flag_checkmate
  end

  # if direction = to - the move is "incoming", else (from) - "outgoing"
  def self.move_valid?(board, coordinate, player_colour, direction)
    figure = board[*coordinate]
    if direction == :to
      figure.nil? || (!figure.nil? && figure.colour != player_colour)
    else
      !figure.nil? && figure.colour == player_colour
    end
  end

  # private

  def self.threats(board, piece)
    piece_coordinates = [piece.position.row, piece.position.column]
    opponent_color = piece.colour == :white ? :black : :white
    board.pieces(opponent_color).select do |opponent|
      opponent.get_moves(board).include?(piece_coordinates)
    end
  end

  def self.can_avoid_threat?(board, attacker, threatened)
    allies = board.pieces(threatened.colour) - [threatened]
    allies.any? do |ally|
      !(ally.get_moves(board) & attacker.get_moves(board)).empty? #TODO: attacker.moves being called too many times
    end
  end
end
