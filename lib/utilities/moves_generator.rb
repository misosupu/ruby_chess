# Generates moves for the chess pieces, depending on the
# piece movement type - either continuous (Bishop, Queen, Rook)
# or discrete (Pawn, King, Knight).
module MovesGenerator

  # Checks whether a specific board tile is occupied by an opponent.
  #
  # @param [Coordinate] position the tile coordinate (in [row, column] format)
  # @param colour of the current chess piece to be moved
  # @return true or false
  def self.opponent_ahead?(board, position, colour)
    return false if board[position.row, position.column].nil?
    board.taken?(position.row, position.column) &&
        board[position.row, position.column].colour != colour
  end

  # Checks whether a specific board tile coordinate is out of bounds.
  def self.out_of_bounds?(row, column)
    row < 0 || column < 0 || row > 7 || column > 7
  end

  def self.cell_inaccessible?(board, figure, goal, colour)
    # if the pawn tries to capture by moving forward
    return true if out_of_bounds?(goal.row, goal.column)
    return false if board[goal.row, goal.column].nil?
    (board.taken?(goal.row, goal.column) &&
        ((board[goal.row, goal.column].colour == colour) ||
        (figure.is_a?(Pawn) && figure.position.column == goal.column))
    )
  end

  module DiscreteMovesGenerator
    def self.moves(board, figure)
      coordinates = []
      colour = figure.colour
      figure.movement_directions.each do |row, column|
        goal = figure.position.clone
        goal.add(row, column)
        unless MovesGenerator.cell_inaccessible?(board, figure, goal, colour)
          coordinates << [goal.row, goal.column]
        end
      end
      if figure.is_a?(Pawn)
        coordinates.concat(pawn_capture_coordinates(board, figure))
      end
      coordinates
    end

    # Unlike other pieces, the pawn does not capture
    # in the same direction as it otherwise moves.
    # A pawn captures diagonally, one square forward and to the left or right.
    def self.pawn_capture_coordinates(board, pawn)
      opponent_coordinates = [[1 * pawn.orientation, -1],
                              [1 * pawn.orientation, 1]]
      opponent_coordinates.map! do |row, column|
        [row + pawn.position.row, column + pawn.position.column]
      end
      opponent_coordinates.select do |row, column|
        position = Coordinate.new(row, column)
        !MovesGenerator.cell_inaccessible?(board, pawn, position, @colour) &&
            MovesGenerator.opponent_ahead?(board, position, @colour)
      end
    end
  end

  module ContinuousMovesGenerator
    def self.moves(board, figure)
      coordinates = []
      figure.movement_directions.each do |row, column|
        position = figure.position.clone
        colour = figure.colour
        loop do
          position.add(row, column)
          break if MovesGenerator.cell_inaccessible?(board, figure, position, colour)
          coordinates << [position.row, position.column]
          if MovesGenerator.opponent_ahead?(board, position, colour)
            break
          end
        end
      end
      coordinates
    end
  end
end