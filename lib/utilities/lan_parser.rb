module NotationParser

  # LAN uses a leading redundant, national dependent uppercase piece letter
  # of the moving piece to represent the move, which makes it more human
  # readable and compatible with SAN and descriptive notation.
  # <LAN move descriptor> ::= <Piece symbol><from square>['-'|'x']<to square>
  # <Piece symbol> ::= 'N' | 'B' | 'R' | 'Q' | 'K'

  module LanParser

    # Transforms the source and target coordinates into LAN notation.
    # source and target are in the form of [row, column]
    # piece holds the chess piece, which is being moved
    def self.parse_move(piece, source, target)
      convert_piece(piece) + convert_coordinate(*source) + '-' +
          convert_coordinate(*target)
    end

    private

    # Transforms coordinates into LAN notation.
    # row and column are numbers from 1 through 8
    def self.convert_coordinate(row, column)
      row.to_s + column.to_s
    end

    # Transforms the piece into LAN notation.
    def self.convert_piece(piece)
      piece.class.name[0].downcase
    end
  end
end