require_relative 'notation_parser'

module NotationParser

  # Facilitates conversion from FEN string to board and the reverse. Mappings:
  # pawn = "P", knight = "N", bishop = "B", rook = "R", queen = "Q", king = "K"
  # White pieces are designated using upper-case letters (PNBRQK) while black
  # pieces use lowercase (pnbrqk)

  module FenParser
    def self.fen_to_board(fen_string)
      rows = fen_string.split('/')
      rows.map.with_index { |row, number| parse_row(row, number).flatten }
    end

    def self.board_to_fen(board)
      fen_string = String.new
      board.each do |row|
        consecutive_empty_tiles = 0
        row.each_with_index do |current_element, index|
          if current_element.is_a?(Piece)
            if index > 0 && consecutive_empty_tiles > 0
              fen_string << consecutive_empty_tiles.to_s
              consecutive_empty_tiles = 0
            end
              piece_type = NotationParser::FEN_NOTATION.invert[current_element.class.name]
              fen_string << (current_element.colour == :white ? piece_type : piece_type.downcase)
          elsif current_element.nil?
            consecutive_empty_tiles += 1
          end
          if index == 7 && consecutive_empty_tiles > 0
            fen_string << consecutive_empty_tiles.to_s
          end
        end
        fen_string << '/'
      end
      fen_string.chop
    end

    private

    def self.parse_row(row, row_number)
      row.chars.map.with_index do |element, column|
        parse_element(element, row_number, column)
      end
    end

    def self.standard_to_coords(standard)
      coordinates = standard.chars
      coordinates[0] = coordinates[0].ord - 97
      coordinates[1] = 8 - coordinates[1].to_i
      [coordinates[1], coordinates[0]]
    end

    def self.parse_element(symbol, row, column)
      if symbol =~ /^[1-8]$/
        parsed = Array.new(Integer(symbol), nil)
      else
        colour = /[[:upper:]]/.match(symbol) ? :white : :black
        piece_type = NotationParser::FEN_NOTATION[symbol.upcase]
        parsed = Object.const_get(piece_type).new(row, column, colour)
      end
      parsed
    end
  end
end