# Facilitates monitoring of the moves played to account for
# the three special rules - castling, en passant and promotion
module RuleInspector

  # checks if a piece meets condition(s) for special movement(s)
  def self.identify_special_move(board, piece)
    if piece.is_a?(Pawn)
      # check for en passant and promotion
      puts 'TODO'
    end
  end

  # It can only occur when a player exercises his option to move his pawn
  # two squares on its initial movement and that move places his pawn next
  # to the opponent's pawn. When this happens, the opposing player has the
  # option to use his pawn to take the moved pawn "en passant" or "in passing"
  # as if the pawn had only moved one square.
  # This option only stays open for one move.
  def self.en_passant?(board, pawn)
    opponent_pawns = neighbours(pawn, board)
    pawn.used_two_square_advance?
  end

  # Castling consists of moving the king two squares towards a rook on the
  # player's first rank, then moving the rook to the square over which the king
  # crossed.Castling may only be done if the king has never moved, the rook
  # involved has never moved, the squares between the king and the rook involved
  # are unoccupied, the king is not in check, and the king does not cross over
  # or end on a square in which it would be in check.
  def self.castling?(board, colour)
    king = board.locate_king(colour)
    rooks = board.values.find { |tile| tile.is_a? Rook  && tile.colour == colour }
    rooks.each do |rook|
      if king.rank == 1 && rook.rank == 1 && (king.column - rook.column).abs == 2
        return true
      end
    end
    false
  end

  # Promotion is a chess rule that a pawn that reaches its eighth rank is
  # immediately changed into the player's choice of a queen, knight, rook,
  # or bishop of the same color. The new piece replaces the pawn on the
  # same square, as part of the same move.
  def self.promotion?(board, pawn)
    (pawn.colour == :white && pawn.position.row == 7) ||
        (pawn.colour == :black && pawn.position.row == 0)
  end

  private

  # returns figures to the left and right of current pawn
  def neighbours(pawn, board)
    neighbours = [board[pawn.row, pawn.column - 1],
                  board[pawn.row, pawn.column + 1]]
    neighbours.select { |figure| !figure.nil && figure.is_a?(:Pawn) &&
                        figure.colour != pawn.colour }
  end
end