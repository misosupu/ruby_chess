class ChessError < StandardError
  def initialize(msg = 'Chess Error')
    super
  end
end
