# Keeps track of pieces current position, defined by row and column.
class Coordinate
  attr_accessor :row, :column
  
  def initialize(row, column)
    @row = row
    @column = column
  end

  def ==(other)
    row == other.row && column == other.column
  end

  def change(row, column)
    @row = row
    @column = column
  end

  def add(direction_x, direction_y)
      @row += direction_x
      @column += direction_y
  end
end