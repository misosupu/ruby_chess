# Parses input for the console view.
module InputParser

  # Validates if the input is in correct form
  def self.validate_input(input)
    input =~ /^[1-8][1-8]$/
  end

  # Validates if the decision is a number from 1 to 3
  def self.decision_valid?(input, type=:turn)
    upper_bound = 4
    if type == :newgame
      upper_bound = 2
    end
    regex = Regexp.new("^[1-#{upper_bound}]$")
    input =~ regex
  end

  # Transforms input, based on its type from string to
  # [row, column], when input has been previously validated
  def self.parse(input)
    input = input.chars
    row = input[0].to_i - 1
    col = input[1].to_i - 1
    [row, col]
  end
end