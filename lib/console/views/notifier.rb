module ActionNotifier
  def self.announce_player(current_player)
    puts "Player #{current_player}'s turn."
  end

  def self.prompt_new_game
    puts 'Would you like to start a new game (1) or load a saved one (2)?'
  end

  def self.prompt_game_name
    puts 'Please, enter game name: '
  end

  def self.load_game(game_name)
    puts "Loading game #{game_name}..."
  end

  def self.prompt_decision # TODO: rename more accurately
    puts "What would you like to do?\n" \
         "1. Save current game\n2. Resign\n3. Play\n4. Exit"
  end

  def self.announce_resign(players, player)
    winner = players.find { |colour| colour != player }.to_s
    puts "Player #{player} has resigned. Player #{winner} wins."
  end

  def self.game_over
    puts 'Game over'
  end

  def self.announce_savegame(filename)
    puts "Your game has been saved as #{filename}"
  end

  def self.prompt_starting_coordinate
    puts 'Please, enter coordinate of the figure you would like to move: '
  end

  def self.prompt_goal_coordinate
    puts 'Please, enter coordinate of the tile you would like'  \
         ' to move the figure to: '
  end

  def self.wrong_input
    puts "You've entered a wrong value. " \
          'Please, try again.'
  end

  def self.check
    puts "Unable to move to said position - you're in check!" \
          'Please, try again:'
  end

  def self.checkmate(player)
    puts "Player #{player} is in check. Game Over."
  end
end
