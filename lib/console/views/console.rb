require 'colorize'

# Facilitates displaying the board and pieces when in console mode.
module ConsoleView

  PIECES = { Rook: '♜', Bishop: '♝', Pawn: '♟',
             King: '♚', Queen: '♛', Knight: '♞' }

  MARGIN = '  '

  def self.render(board)
    grid = copy_board(board)
    annotate(grid)
    rendered = []
    grid.each_with_index do |row, index|
      rendered << render_row(row, index)
    end
    print rendered.join("\n")
    print "\n"
  end

  def self.render_row(row, index)
    return MARGIN + row.join(' ') if index == 0 # upper board
    tile_background = index.even? ? [:red, :blue] : [:blue, :red]
    tile_background = tile_background.cycle
    row.map { |piece| render_piece(piece, tile_background) }.join('')
  end

  # Draws a game piece
  def self.render_piece(piece, tile_background)
    background_color = tile_background.next
    colored_piece = piece
    if piece.nil?
      colored_piece = MARGIN.colorize(background: background_color)
    elsif piece.is_a?(Piece)
      colored_piece = " #{PIECES[piece.class.name.to_sym].colorize(piece.colour)}".colorize(background: background_color)
    end
    colored_piece
  end

  # Prints row and column numbers on the board borders
  def self.annotate(grid)
    grid.each_with_index { |row, number| row.unshift(number + 1) }
    grid.unshift((1..8).to_a)
  end

  private

  def self.copy_board(board)
    grid = []
    board.board.each do |row|
      grid << row.clone
    end
    grid
  end
end