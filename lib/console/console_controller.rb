require_relative 'input_parser'
require_relative '../game_engine'
require_relative '../utilities/chess_exceptions'
require_relative '../utilities/board_inspector'
require_relative 'views/console'
require_relative 'views/notifier'


class ConsoleController
  attr_accessor :game_engine

  def initialize(initial_state = nil)
    @game_engine = GameEngine.new(initial_state)
  end

  def start
    ActionNotifier.prompt_new_game
    decision = gets.chomp
    until InputParser.decision_valid?(decision, :newgame)
      ActionNotifier.wrong_input
      decision = gets.chomp
    end
    decision = Integer(decision)
    decision == 1 ? play : load_game
  end

  def load_game
    ActionNotifier.prompt_game_name
    game_name = gets.chomp
    ActionNotifier.load_game(game_name)
    @game_engine.load_game(game_name)
    play
  end

  def play(loaded=false)
    loop do
      if @game_engine.game_over?
        ActionNotifier.checkmate(@game_engine.current_player)
        break
      end
      ActionNotifier.announce_player(@game_engine.current_player)
      decision = decide_move
      case decision
        when 1
          filename = @game_engine.save_game
          ActionNotifier.announce_savegame(filename.split('/')[-1])
        when 2
          ActionNotifier.announce_resign(@game_engine.players, @game_engine.current_player)
          Kernel.exit
        when 4
          Kernel.exit
        else
          system 'clear'
          ConsoleView.render(@game_engine.board)
          move
      end
    end
  end

  def decide_move
    ActionNotifier.prompt_decision
    decision = gets.chomp
    until InputParser.decision_valid?(decision)
      ActionNotifier.wrong_input
      decision = gets.chomp
    end
    Integer(decision)
  end

  private

  def move
    begin
      current = get_coordinate(:from)
      goal = get_coordinate(:to)
      begin
        @game_engine.move_figure(current, goal)
      rescue ChessError
        ActionNotifier.check
      end
    rescue ChessError
      ActionNotifier.wrong_input
      retry
    end
  end

  def get_coordinate(direction)
    loop do
      if direction == :from
        ActionNotifier.prompt_starting_coordinate
      else
        ActionNotifier.prompt_goal_coordinate
      end
      coordinate = gets.chomp
      if InputParser.validate_input(coordinate)
        coordinates =  InputParser.parse(coordinate)
        if @game_engine.coordinate_valid?(coordinates, direction)
          return coordinates
        else
          ActionNotifier.wrong_input
        end
      end
    end
  end
end
